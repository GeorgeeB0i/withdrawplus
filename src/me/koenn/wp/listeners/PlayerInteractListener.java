package me.koenn.wp.listeners;

import me.koenn.wp.WithdrawPlus;
import me.koenn.wp.util.*;
import me.koenn.wp.voucher.ExpVoucher;
import me.koenn.wp.voucher.MoneyVoucher;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.ItemStack;

import static net.md_5.bungee.api.ChatColor.translateAlternateColorCodes;

public class PlayerInteractListener implements Listener {

    private String MONEY_SUCCEED;
    private String EXP_SUCCEED;

    public PlayerInteractListener() {
        MONEY_SUCCEED = translateAlternateColorCodes('&', ConfigManager.getString("MONEY_DEPOSIT", "commandMessages"));
        EXP_SUCCEED = translateAlternateColorCodes('&', ConfigManager.getString("EXP_DEPOSIT", "commandMessages"));
    }

    @EventHandler
    public void onPlayerInteract(PlayerInteractEvent e) {
        Player player = e.getPlayer();
        if (Util.getItemInHand(player) == null) {
            return;
        }
        ItemStack item = Util.getItemInHand(player);
        if (!item.hasItemMeta()) {
            return;
        }
        if (!item.getItemMeta().hasDisplayName()) {
            return;
        }
        if (!item.getItemMeta().hasLore()) {
            return;
        }
        if (item.getType().equals(ItemUtil.getMoneyMaterial())) {
            e.setCancelled(true);
            WithdrawPlus.econ.depositPlayer(player, MoneyVoucher.getAmountFromItem(item));
            player.playSound(player.getLocation(), SoundUtil.getMoneySound(), 1, 1);
            Util.removeItemInHand(player);
            for (String message : MONEY_SUCCEED.replace("{amount}", String.valueOf(MoneyVoucher.getAmountFromItem(item))).split("%")) {
                player.sendMessage(ChatColor.translateAlternateColorCodes('&', message));
            }
        } else if (item.getType().equals(ItemUtil.getExpMaterial())) {
            e.setCancelled(true);
            ExpUtil.setTotalExperience(player, ExpUtil.getTotalExperience(player) + ExpVoucher.getAmountFromItem(item));
            player.playSound(player.getLocation(), SoundUtil.getExpSound(), 1, 1);
            Util.removeItemInHand(player);
            for (String message : EXP_SUCCEED.replace("{amount}", String.valueOf(ExpVoucher.getAmountFromItem(item))).split("%")) {
                player.sendMessage(ChatColor.translateAlternateColorCodes('&', message));
            }
        }
    }
}
