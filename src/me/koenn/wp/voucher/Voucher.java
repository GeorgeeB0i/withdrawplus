package me.koenn.wp.voucher;

import me.koenn.wp.util.ConfigManager;
import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import java.util.List;
import java.util.stream.Collectors;

import static net.md_5.bungee.api.ChatColor.translateAlternateColorCodes;

/**
 * General Voucher object.
 */
public abstract class Voucher {

    private Material material;
    private String name; //Used to get config values.
    private int amount;
    private String ownerName;
    private ItemStack voucher;

    /**
     * Voucher constructor.
     *
     * @param material  Material used for the voucher
     * @param name      Name of the voucher
     * @param amount    Value of the voucher
     * @param ownerName Owner of the voucher
     */
    Voucher(Material material, String name, int amount, String ownerName) {
        this.material = material;
        this.name = name;
        this.amount = amount;
        this.ownerName = ownerName;
        this.voucher = mkVoucher(this);
    }

    /**
     * Make the voucher into an ItemStack.
     *
     * @param v Voucher object instance
     * @return ItemStack voucher
     */
    private static ItemStack mkVoucher(Voucher v) {
        ItemStack voucher = new ItemStack(v.getMaterial(), 1);
        ItemMeta meta = voucher.getItemMeta();
        String title = ConfigManager.getString("title", v.getName());
        List<String> l = ConfigManager.getList("lore", v.getName());
        List<String> lore = l.stream().map(s -> translateAlternateColorCodes('&', s)
                .replace("{amount}", String.valueOf(v.getAmount()))
                .replace("{owner}", v.getOwnerName())).collect(Collectors.toList());
        meta.setDisplayName(translateAlternateColorCodes('&', title));
        meta.setLore(lore);
        voucher.setItemMeta(meta);
        return voucher;
    }

    //All getters and setters for the Voucher object.

    public ItemStack getVoucher() {
        return voucher;
    }

    private Material getMaterial() {
        return material;
    }

    private String getName() {
        return name;
    }

    public int getAmount() {
        return amount;
    }

    private String getOwnerName() {
        return ownerName;
    }
}
