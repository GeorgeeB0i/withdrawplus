package me.koenn.wp.commands.commands;

import me.koenn.wp.WithdrawPlus;
import me.koenn.wp.commands.WithdrawCommand;
import me.koenn.wp.util.ConfigManager;
import me.koenn.wp.util.MinMaxUtil;
import me.koenn.wp.util.SoundUtil;
import me.koenn.wp.voucher.MoneyVoucher;
import org.bukkit.ChatColor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import static net.md_5.bungee.api.ChatColor.translateAlternateColorCodes;

public class WithdrawMoney implements WithdrawCommand {

    private String NOT_ENOUGH_MONEY;
    private String SUCCEED;
    private String MAX;
    private String MIN;

    public WithdrawMoney() {
        NOT_ENOUGH_MONEY = translateAlternateColorCodes('&', ConfigManager.getString("NOT_ENOUGH_MONEY", "commandMessages"));
        SUCCEED = translateAlternateColorCodes('&', ConfigManager.getString("MONEY_WITHDRAW", "commandMessages"));
        MAX = translateAlternateColorCodes('&', ConfigManager.getString("MONEY_MAX", "commandMessages"));
        MIN = translateAlternateColorCodes('&', ConfigManager.getString("MONEY_MIN", "commandMessages"));
    }

    @Override
    public boolean execute(CommandSender sender, String[] args) {
        if (args.length < 1) {
            return false;
        }
        if (!(sender instanceof Player)) {
            return false;
        }
        Player player = (Player) sender;
        int amount;
        try {
            amount = Integer.parseInt(args[0]);
        } catch (NumberFormatException ex) {
            if (args[0].equalsIgnoreCase("all")) {
                amount = (int) Math.round(WithdrawPlus.econ.getBalance(player));
            } else {
                return false;
            }
        }
        if (amount > MinMaxUtil.getMaxMoney()) {
            for (String message : MAX.split("%")) {
                sender.sendMessage(ChatColor.translateAlternateColorCodes('&', message));
            }
            return true;
        }
        if (amount < MinMaxUtil.getMinMoney()) {
            for (String message : MIN.split("%")) {
                sender.sendMessage(ChatColor.translateAlternateColorCodes('&', message));
            }
            return true;
        }
        if (amount < 1) {
            return false;
        }
        if (WithdrawPlus.econ.getBalance(player) < amount) {
            for (String message : NOT_ENOUGH_MONEY.split("%")) {
                sender.sendMessage(ChatColor.translateAlternateColorCodes('&', message));
            }
            return true;
        }
        WithdrawPlus.econ.withdrawPlayer(player, amount);
        MoneyVoucher voucher = new MoneyVoucher(amount, player.getName());
        player.getInventory().addItem(voucher.getVoucher());
        for (String message : SUCCEED.replace("{amount}", String.valueOf(amount)).split("%")) {
            sender.sendMessage(ChatColor.translateAlternateColorCodes('&', message));
        }
        player.playSound(player.getLocation(), SoundUtil.getMoneySound(), 1, 1);
        return true;
    }

    @Override
    public String getName() {
        return "withdrawMoney";
    }

    @Override
    public String getUsage() {
        return ConfigManager.getString("MONEY_USAGE", "commandMessages");
    }
}
