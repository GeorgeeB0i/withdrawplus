package me.koenn.wp.util;

import org.bukkit.Sound;

/**
 * Utility class for the voucher sounds.
 */
public class SoundUtil {

    private static Sound MONEY_SOUND;
    private static Sound EXP_SOUND;

    /**
     * Setup the static Sound variables for the voucher items.
     */
    public static void setupSounds() {
        MONEY_SOUND = Sound.valueOf(ConfigManager.getString("MONEY_SOUND", "sounds").toUpperCase());
        EXP_SOUND = Sound.valueOf(ConfigManager.getString("EXP_SOUND", "sounds").toUpperCase());
    }

    //Getters for the static Sound variables.

    public static Sound getMoneySound() {
        return MONEY_SOUND;
    }

    public static Sound getExpSound() {
        return EXP_SOUND;
    }
}
