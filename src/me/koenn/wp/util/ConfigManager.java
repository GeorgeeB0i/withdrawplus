package me.koenn.wp.util;

import me.koenn.wp.WithdrawPlus;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.configuration.file.FileConfiguration;

import java.io.File;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Manager class for the config.yml file.
 * Handles reading and writing to the file and saves automatically.
 */
public class ConfigManager {

    private static FileConfiguration config;
    private static WithdrawPlus main;

    @SuppressWarnings("ResultOfMethodCallIgnored")
    public ConfigManager(FileConfiguration c, WithdrawPlus m) {
        config = c;
        main = m;
    }

    public static void setString(String value, String key, String... path) {
        createSection(path).set(key, value);
        main.saveConfig();
    }

    public static String getString(String key, String... path) {
        return getSection(path).get(key).toString();
    }

    public static void setList(List<String> list, String key, String... path) {
        createSection(path).set(key, list);
        main.saveConfig();
    }

    public static List<String> getList(String key, String... path) {
        return getSection(path).getList(key).stream().map(Object::toString).collect(Collectors.toList());
    }

    private static ConfigurationSection createSection(String... path) {
        ConfigurationSection section = null;
        for (String p : path) {
            if (section != null) {
                if (section.getConfigurationSection(p) == null) {
                    section = section.createSection(p);
                } else {
                    section = section.getConfigurationSection(p);
                }
            } else {
                if (config.getConfigurationSection(p) == null) {
                    section = config.createSection(p);
                } else {
                    section = config.getConfigurationSection(p);
                }
            }
        }
        if (section == null) {
            throw new IllegalArgumentException("Section cannot be null");
        }
        return section;
    }

    private static ConfigurationSection getSection(String... path) {
        ConfigurationSection section = null;
        for (String p : path) {
            if (section != null) {
                section = section.getConfigurationSection(p);
            } else {
                section = config.getConfigurationSection(p);
            }
        }
        if (section == null) {
            throw new IllegalArgumentException("Section cannot be null");
        }
        return section;
    }

    public static FileConfiguration getConfiguration() {
        return config;
    }

    @SuppressWarnings("ResultOfMethodCallIgnored")
    public void setupConfig() {
        if (!main.getDataFolder().exists()) {
            main.getDataFolder().mkdir();
        }
        if (!(new File(main.getDataFolder(), "config.yml")).exists()) {
            main.saveDefaultConfig();
        }
    }
}
