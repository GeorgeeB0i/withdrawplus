package me.koenn.wp.util;

/**
 * Enum for the different voucher types.
 */
public enum VoucherType {

    MONEY, EXP;
}
