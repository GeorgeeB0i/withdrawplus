package me.koenn.wp.util;

import org.bukkit.entity.Player;

/**
 * Utility class for experience modifying.
 */
public class ExpUtil {

    /**
     * Sets a player's total experience.
     *
     * @param player Player object instance
     * @param exp    int amount of experience
     */
    public static void setTotalExperience(Player player, int exp) {
        player.setExp(0.0F);
        player.setLevel(0);
        player.setTotalExperience(0);

        int amount = exp;
        while (amount > 0) {
            int expToLevel = getExpAtLevel(player.getLevel());
            amount -= expToLevel;
            if (amount >= 0) {
                player.giveExp(expToLevel);
            } else {
                amount += expToLevel;
                player.giveExp(amount);
                amount = 0;
            }
        }
    }

    /**
     * Gets the experience count at a certain level.
     *
     * @param level int level
     * @return int experience
     */
    private static int getExpAtLevel(int level) {
        if (level <= 15) {
            return 2 * level + 7;
        }
        if ((level >= 16) && (level <= 30)) {
            return 5 * level - 38;
        }
        return 9 * level - 158;
    }

    /**
     * Gets a player's total experience.
     *
     * @param player Player object instance
     * @return int amount of experience
     */
    public static int getTotalExperience(Player player) {
        int exp = Math.round(getExpAtLevel(player.getLevel()) * player.getExp());
        int currentLevel = player.getLevel();
        while (currentLevel > 0) {
            currentLevel--;
            exp += getExpAtLevel(currentLevel);
        }
        if (exp < 0) {
            exp = Integer.MAX_VALUE;
        }
        return exp;
    }

}
