# **WithdrawPlus** #
![img.png](https://bitbucket.org/repo/KpgkbX/images/1893507768-img.png)
## **A customizable withdraw plugin for Money and Exp.** ##
**If you want to use this plugin for 1.8, the default config won't work, use [this](https://www.dropbox.com/s/bwgoam97rtq53ss/config.yml?dl=1) one!**

Use */withdrawMoney <amount>* to withdraw money from any vault economy plugin. 

Use */withdrawXp <amount>* to withdraw exp. 

Simply rightclick the voucher you get from the commands to redeem the money/exp.


The tooltips, the commands, the items, the sounds and the messages are customizable in the config file.
You can use /givevoucher command to get a voucher signed by the server.

If you have a request for this plugin, just pm me.